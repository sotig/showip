package main

import (
	"fmt"
	// "net/http"
	"routes"
	"server"
)

func main() {
	fmt.Println("Starting server")
	app := server.Init()

	// app.AddMiddleware(func(next http.Handler) http.Handler {
	// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 		fmt.Println("0")
	// 		next.ServeHTTP(w, r)
	// 	})
	// })

	// app.AddMiddleware(func(next http.Handler) http.Handler {
	// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 		fmt.Println("1")
	// 		next.ServeHTTP(w, r)
	// 	})
	// })

	routes.Register(app)

	fmt.Println("Serving 2007")
	app.Serve(2007)
}

FROM golang:latest
 
WORKDIR /usr/local/go/

COPY . .
  
RUN go env -w GO111MODULE=auto

RUN go build -o main server.go

EXPOSE 2007:2007 

CMD ["/usr/local/go/main"]
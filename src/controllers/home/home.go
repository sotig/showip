package home

import (
	"io"
	"net/http"
)

func ReadUserIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	return IPAddress
}

func Index(w http.ResponseWriter, req *http.Request) {
	Ip := ReadUserIP(req)

	io.WriteString(w, "Your ip is: "+Ip)
}

// func Post(w http.ResponseWriter, req *http.Request) {
// 	type User struct {
// 		Name string `json:"name"`
// 	}

// 	user := &User{Name: "Frank"}

// 	json_response.WriteJSON(w, &user)
// }

// func HelloServer(w http.ResponseWriter, req *http.Request) {
// 	name := req.URL.Query().Get(":name")

// 	io.WriteString(w, "hello, "+name+"!\n")
// }

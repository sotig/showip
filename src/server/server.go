package server

import (
	"log"
	"net/http"
	"strconv"

	"github.com/bmizerany/pat"
)

type MiddlewareHandler func(http.Handler) http.Handler
type PolicyHandler func(*http.Request) bool

type Server struct {
	pt         *pat.PatternServeMux
	middleware []MiddlewareHandler
}

func Init() *Server {
	patMux := pat.New()

	return &Server{pt: patMux, middleware: []MiddlewareHandler{}}
}

func (ser *Server) AddMiddleware(handler MiddlewareHandler) {
	ser.middleware = append(ser.middleware, handler)
}

func _registerMiddleware(next http.Handler, policy PolicyHandler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if policy(r) {
			next.ServeHTTP(w, r)
		} else {
			http.Error(w, "Unauthorized", 401)
		}
	})
}

func (ser *Server) Handle(method string, pattern string, handler http.HandlerFunc, policy PolicyHandler) {

	finalHandler := http.HandlerFunc(handler)

	middlewareHandler := _registerMiddleware(finalHandler, policy)

	for i := len(ser.middleware) - 1; i >= 0; i-- {
		middlewareHandler = ser.middleware[i](middlewareHandler)
	}

	if method == "GET" {
		ser.pt.Get(pattern, middlewareHandler)
	} else if method == "POST" {
		ser.pt.Post(pattern, middlewareHandler)
	} else if method == "PUT" {
		ser.pt.Put(pattern, middlewareHandler)
	} else if method == "DELETE" {
		ser.pt.Del(pattern, middlewareHandler)
	} else if method == "OPTIONS" {
		ser.pt.Options(pattern, middlewareHandler)
	} else if method == "PATCH" {
		ser.pt.Patch(pattern, middlewareHandler)
	}

}

func (ser *Server) Serve(port int) {

	http.Handle("/", ser.pt)

	err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

package routes

import (
	"controllers/home"
	"policies"
	"server"
)

func Register(app *server.Server) {
	app.Handle("GET", "/", home.Index, policies.AllowAll)
	// app.Handle("POST", "/post-test", home.Post, policies.AllowAll)
	// app.Handle("GET", "/hello/:name", home.HelloServer, policies.AllowAll)
}
